from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.serializers import StripSerializer, CategorySerializer
from core.models import Strip, Category


class StripViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Strip.objects.all()
    serializer_class = StripSerializer

    @action(detail=False, methods=["GET"])
    def random(self, request):
        instance = Strip.objects.random()
        return self._serialize_and_return(instance)

    def _serialize_and_return(self, instance):
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
