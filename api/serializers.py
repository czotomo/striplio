from rest_framework.serializers import ModelSerializer
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField

from core.models import Category, Strip


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category


class StripSerializer(ModelSerializer):
    thumbnail = HyperlinkedSorlImageField(
        "500x500", options={"crop": "center"}, source="image"
    )

    class Meta:
        fields = ("id", "title", "thumbnail")
        model = Strip
