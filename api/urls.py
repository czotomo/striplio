from django.conf.urls import url, include
from rest_framework import routers
from api import views as api_views

router = routers.DefaultRouter()
router.register(r"strips", api_views.StripViewSet, base_name="strips_list")

urlpatterns = [url(r"^api/", include(router.urls))]
