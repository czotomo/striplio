import uuid

from django.db import models
from vote.models import VoteModel

from sorl.thumbnail import ImageField

from core.managers import StripManager


class BaseModel(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False, db_index=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ("-created_at",)


class Category(BaseModel):
    name = models.TextField(max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"


class Strip(BaseModel, VoteModel):
    title = models.TextField(max_length=64)
    image = ImageField(upload_to="strip_imgs")
    categories = models.ManyToManyField(Category)
    tooltip = models.TextField(max_length=128, null=True)
    hidden = models.BooleanField(default=False)

    objects = StripManager()

    def __str__(self):
        return self.title
