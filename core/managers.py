from random import randint

from django.db import models


class StripManager(models.Manager):
    def random(self):
        cnt = self.count()
        return self.all()[randint(1, cnt - 1)]
