from django.contrib import admin

from core.models import Strip, Category


class StripAdmin(admin.ModelAdmin):
    list_display = ("title", "strip_categories", "tooltip", "image")

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("categories")

    def strip_categories(self, obj):
        return ", ".join([cat.name for cat in obj.categories.all()])


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Strip, StripAdmin)
admin.site.register(Category, CategoryAdmin)
