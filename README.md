## Running
### Setup
- frontend
    `npm install`
- backend
    `pipenv install` (or without pipenv, whatever)
    `python manage.py migrate` in venv

### Running the project
- `pipenv install --dev`
- `npm install`
- `npm run start`
- `pipenv shell`
- `python manage.py runserver`

### Testing
`make test`

runs django tests using `--keepdb` and `--parallel`. You may pass a path to the desired test module in the make command. E.g.:

## Linting
- Manually with `prospector` and `npm run lint` on project root.
