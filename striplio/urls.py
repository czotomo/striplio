from django.conf import settings
from django.conf.urls import include, url  # noqa
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from api.urls import urlpatterns as api_urls
from core.views import IndexView

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^$", IndexView.as_view(), name="home"),
]

urlpatterns += api_urls

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
