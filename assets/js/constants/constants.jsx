export const STRIP_DETAIL_API_ROUTE = "/api/strips";
export const RANDOM_STRIP_API_ROUTE = "/api/strips/random";
export const STRIPS_API_LIST_ROUTE = "/api/strips/";
