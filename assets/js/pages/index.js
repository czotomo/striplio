import React from "react";
import ReactDOM from "react-dom";
import App from "../app/striplio-gallery";
import { Provider } from "mobx-react";
import StripsStore from "../app/striplio-gallery/stores/StripsStore";

const Root = (
    <Provider store={StripsStore}>
        <App />
    </Provider>
);

ReactDOM.render(Root, document.getElementById("react-app"));

if (module.hot) {
    module.hot.accept("../app/striplio-gallery", () => {
        ReactDOM.render(Root, document.getElementById("react-app"));
    });
}
