import React from "react";
import { Button, Col, Row } from "reactstrap";
import "./ImgControl.scss";

class ImgControl extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {
            handleClickLeft,
            handleClickRight,
            handleClickRandom,
            leftButtonDisabled,
            rightButtonDisabled
        } = this.props;

        return (
            <Row>
                <Col>
                    <Button
                        onClick={handleClickLeft}
                        color="primary"
                        disabled={leftButtonDisabled}
                        className="s-btn"
                    >
                        Previous
                    </Button>
                </Col>
                <Col>
                    <Button
                        onClick={handleClickRandom}
                        color="primary"
                        className="s-btn"
                    >
                        Random
                    </Button>
                </Col>
                <Col>
                    <Button
                        onClick={handleClickRight}
                        color="primary"
                        disabled={rightButtonDisabled}
                        className="s-btn"
                    >
                        Next
                    </Button>
                </Col>
            </Row>
        );
    }
}

export default ImgControl;
