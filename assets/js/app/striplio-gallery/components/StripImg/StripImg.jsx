import React from "react";
import "./StripImg.scss";
import { Col, Row } from "reactstrap";
import {inject, observer} from "mobx-react";

@inject("store")
@observer
class StripImg extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col>
                    <img img={this.props.imgSrc} className="img-fluid" alt="??" />
                </Col>
            </Row>
        );
    }
}

export default StripImg;
