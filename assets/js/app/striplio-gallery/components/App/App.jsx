import React from "react";
import Header from "../Header";
import ImgDisplay from "../ImgDisplay";
import Footer from "../Footer";

import "./App.scss";
import { Col, Container, Row } from "reactstrap";
import {inject, observer} from "mobx-react";


@inject("store")
@observer
class App extends React.Component {
    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col md={{ size: 6, offset: 3 }}>
                        <Header />
                        <ImgDisplay />
                        <Footer />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default App;
