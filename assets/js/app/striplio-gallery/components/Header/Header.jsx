import React from "react";
import "./Header.scss";
import { Col, Row } from "reactstrap";
import {inject, observer} from "mobx-react";

@inject("store")
@observer
class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col>
                    <h1 className="text-center">{this.props.store.currentStrip.title}</h1>
                </Col>
            </Row>
        );
    }
}

export default Header;
