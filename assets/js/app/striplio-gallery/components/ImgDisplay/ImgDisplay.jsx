import React from "react";
import StripImg from "../StripImg";
import ImgControl from "../ImgControl";
import "./ImgDisplay.scss";
import { Col, Row } from "reactstrap";
import {inject, observer} from "mobx-react";

@inject("store")
@observer
class ImgDisplay extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.props.store.fetchStrips();
    }

    render() {
        return (
            <Row>
                <Col>
                    <StripImg imgSrc={this.props.store.currentStrip.thumb} />
                    <ImgControl
                        rightButtonDisabled={false}
                        leftButtonDisabled={false}
                        handleClickLeft={() => {}}
                        handleClickRight={()=>{}}
                        handleClickRandom={() => {}}
                        currentStripId={() => ""}
                    />
                </Col>
            </Row>
        );
    }
}

export default ImgDisplay;
