import {action, observable} from "mobx";
import {STRIPS_API_LIST_ROUTE, STRIP_DETAIL_API_ROUTE} from "../../../constants/constants";

class StripsStore {
    @observable strips = [];
    @observable currentStrip = {};

    @action fetchStrips() {
        fetch(STRIPS_API_LIST_ROUTE)
            .then(res => res.json())
            .then(
                results => {
                    this.strips = results.map(result => ({
                        thumb: result.thumbnail,
                        title: result.title
                    }));
                },
                error => {
                    alert(error);
                }
            );
    };

    @action fetchStripById(id) {
        fetch(STRIP_DETAIL_API_ROUTE + '/' + id)
            .then(res => res.json())
            .then(result => {
                    this.currentStrip =  {
                        thumb: result.thumbnail,
                        title: result.title
                    }
                },
                error => {
                    alert(error);
                });
    };
}

export default new StripsStore();
